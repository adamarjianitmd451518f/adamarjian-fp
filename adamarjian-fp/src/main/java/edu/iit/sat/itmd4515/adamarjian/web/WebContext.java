/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.adamarjian.web;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 *
 * @author adama
 */
public class WebContext implements ServletContextListener {
    
    
    private ServletContext context = null;
    
    
    
    

    @Override
    public void contextInitialized(ServletContextEvent arg0) {  
        context = arg0.getServletContext();
    }

    @Override
    public void contextDestroyed(ServletContextEvent arg0) {
        context = arg0.getServletContext();

    }
    
}
