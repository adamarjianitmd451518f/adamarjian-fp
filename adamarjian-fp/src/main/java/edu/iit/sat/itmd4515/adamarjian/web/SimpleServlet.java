/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.adamarjian.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/home"},
        initParams = {@WebInitParam(name = "ProductName", value = "Welcome Application")})
public class SimpleServlet extends HttpServlet {

    String appName = "My Application";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        if (name != null) {
            resp.setContentType("text/html");
            resp.getWriter().printf("<application>"
                    + "<name>Hello %s</name>"
                    + "<product>%s</product>"
                    + "</application>", name, appName);
        } else {
            resp.getWriter().write("Please enter a name");
        }

    }

    //this doPOST method is for our name input box on the index page
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        if (name != null && name != "") {
            resp.getWriter().printf("<name>Hello %s</name>", name);
        } else {
            //doing a redirect to send out respoce to another page if they did not fill out the form correctly
        }

    }
}
