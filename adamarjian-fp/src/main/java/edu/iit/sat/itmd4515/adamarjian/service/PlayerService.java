/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.adamarjian.service;

import edu.iit.sat.itmd4515.adamarjian.domain.Child;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author adama
 */
@Stateless
public class PlayerService {
    @PersistenceContext(unitName = "itmd4515PU" )
    private EntityManager em;

    /**
     *
     */
    public PlayerService() {
    }
    
    /**
     *
     * @param child
     */
    public void create(Child child){
        em.persist(child);
    }

    /**
     *
     * @param child
     */
    public void update(Child child){
        em.merge(child);
    }

    /**
     *
     * @param child
     */
    public void remove(Child child){
        em.remove(child);
    }

    /**
     *
     * @param id
     * @return
     */
    public Child find(Long id){
       return em.find(Child.class, id);
    }
    
    /**
     *
     * @return
     */
    public List<Child>  findAll(){
       return em.createNamedQuery("Child.findAll", Child.class)
                .getResultList();
    }
}
