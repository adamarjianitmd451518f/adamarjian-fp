/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.adamarjian.domain;

import edu.iit.sat.itmd4515.adamarjian.security.User;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author adama
 */
@Entity
@Table(name = "Parent")
public class Parent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    private String parentName;
    @OneToOne
    @JoinColumn(name = "USERNAME")
    private User User;

    //Many to many relationship with children and parents
    //parents have the owning side of teh relationship
    @ManyToMany(mappedBy = "parents")
    private List<Child> children = new ArrayList<>();

//  Helper method to help managing my Many to Many relationship
    //we will add children to parents
    public void addChildToParents(Child child) {
//        if our list of children does not contain the child being passed in
        if (!this.children.contains(child)) {
//            we add the child object to children list
            this.children.add(child);
        }
        //if the parents list or collection of parents
//        does not contain this child object
        if (!child.getParents().contains(this)) {
//      add the child object being passed in
            child.getParents().add(this);
        }
    }

    public Parent() {
    }

    public Parent(String parentName) {
        this.parentName = parentName;
    }

    /**
     * Get the value of parentName
     *
     * @return the value of parentName
     */
    public String getParentName() {
        return parentName;
    }

    /**
     * Set the value of parentName
     *
     * @param parentName new value of parentName
     */
    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public List<Child> getChildren() {
        return children;
    }

    public void setChildren(List<Child> children) {
        this.children = children;
    }

    public User getUser() {
        return User;
    }

    public void setUser(User User) {
        this.User = User;
    }

}
