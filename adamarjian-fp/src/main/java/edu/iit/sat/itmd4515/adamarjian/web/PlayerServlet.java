/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.adamarjian.web;

import edu.iit.sat.itmd4515.adamarjian.domain.Child;
import edu.iit.sat.itmd4515.adamarjian.domain.Parent;
import edu.iit.sat.itmd4515.adamarjian.service.PlayerService;
import java.io.IOException;
import java.util.Set;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

/**
 *
 * @author adama
 */
@WebServlet(name = "PlayerServlet", urlPatterns = {"/PlayerServlet"})
public class PlayerServlet extends HttpServlet {

    @Resource
    Validator validator;

    @EJB
    PlayerService playerSVC;
    
//    @EJB
//    ParentService parentSVC;

    private static final Logger LOG = Logger.getLogger(PlayerServlet.class.getName());


    /*
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LOG.info("Inside the doGet method");
        
        for(Child child: playerSVC.findAll()){
            LOG.info("The following players exist in your systerm" 
                    + child.toString());
        }
        
//        for(Parent parent: parentSVC.findAll()){
//            LOG.info("The following parent  exist in your systerm" 
//                    + parent.toString());
//        }
//        
        
        
        //this redirects to a new webpage
//        response.sendRedirect("WEB-INF/newPlayerForm.jsp");
        RequestDispatcher dispatcher
                = request.getRequestDispatcher("WEB-INF/confirm.jsp");
        dispatcher.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LOG.info("Inside the doPost method");
        LOG.info("You passed a a parameter called name "
                + request.getParameter("name"));
        LOG.info("You passed a a parameter called email "
                + request.getParameter("email"));
        LOG.info("You passed a a parameter called age "
                + request.getParameter("age"));

        String nameParam = request.getParameter("name");
        String emailParam = request.getParameter("email");
        //Integer ageParam = request.getParameter("age");

        Child child = new Child(nameParam, 40, emailParam);

        Set<ConstraintViolation<Child>> constrainViolations
                = validator.validate(child);
        if (constrainViolations.size() > 0) {
            LOG.info("There is a problem");
            for (ConstraintViolation<Child> bad : constrainViolations) {
                LOG.info(bad.getPropertyPath() + " " + bad.getMessage());
            }
            //set the state

            request.setAttribute("child", child);
            request.setAttribute("email", child);
            //putting things back into the request context
            request.setAttribute("errors", constrainViolations);

            RequestDispatcher dispatcher
                    = request.getRequestDispatcher("WEB-INF/newPlayerForm.jsp");
            dispatcher.forward(request, response);

        } else {
            LOG.info("no problem with input");
            request.setAttribute("child", child);
            RequestDispatcher dispatcher
                    = request.getRequestDispatcher("WEB-INF/confirm.jsp");
            dispatcher.forward(request, response);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
