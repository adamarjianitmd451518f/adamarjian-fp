/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.adamarjian.domain;

import edu.iit.sat.itmd4515.adamarjian.security.User;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 *
 * @author adama
 */
@Entity
@Table(name = "Child")
//sql statment calls that are being utlized in the test database
@NamedQueries({
     @NamedQuery(name = "Child.findAll",
            query = "SELECT c FROM Child c")
})
public class Child {

    @Id
    @GeneratedValue
    private Long id;

//this annotaion checks if the field is blank
    @NotBlank
    private String childName;
    private Integer age;
    @OneToOne
    @JoinColumn(name = "USERNAME")
    private User user;

    @Email
    private String email;

    @OneToMany(mappedBy = "child")
    private List<Game> games = new ArrayList<>();

    //Many to many relationship between children and parents
    @ManyToMany
    //This creates a new table calles Parents_IDS and
    //adds two columns that areconnectioned to the parents and
//    child lists we created earleir
    @JoinTable(name = "Parents_IDS",
            joinColumns = @JoinColumn(name = "Parent_ID"),
            inverseJoinColumns = @JoinColumn(name = "Child_ID"))
    private List<Parent> parents = new ArrayList<>();

//    creating a helping method to add a child to a game
    public void addParentToChild(Parent parent) {
//       If the child collection does not contain a parent
        if (!this.parents.contains(parent)) {
            this.parents.add(parent);
        }
        if (!parent.getChildren().contains(this)) {
            parent.getChildren().add(this);
        }
    }

    public Child() {
    }

    public Child(String childName, Integer age, String email) {
        this.childName = childName;
        this.age = age;
        this.email = email;
    }

    /**
     * Get the value of id
     *
     * @return the value of id
     */
    public Long getId() {
        return id;
    }

    /**
     * Set the value of id
     *
     * @param id new value of id
     */
    public void setId(Long id) {
        this.id = id;
    }

    public String getChildName() {
        return childName;
    }

    public void setChildName(String name) {
        this.childName = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public List<Game> getGame() {
        return games;
    }

    public void setGame(List<Game> game) {
        this.games = game;
    }

    @Override
    public String toString() {
        return "Child{" + ", childName=" + childName + ", age=" + age + '}';
    }

    public List<Parent> getParents() {
        return parents;
    }

    public void setParents(List<Parent> parents) {
        this.parents = parents;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
