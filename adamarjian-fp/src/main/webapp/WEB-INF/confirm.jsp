<%-- 
    Document   : newplayerform
    Created on : Oct 22, 2018, 11:56:02 AM
    Author     : adama
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <title>Confirmation Page</title>
    </head>
    <body>
        <h1>Player Successfully Validated!</h1>
        <ul>
            <li>
                  <%-- we are getting the actal variable name out of our player.java file
                *it is like calling the player.getName property
                --%>
                ${requestScope.child.childName}          
            </li>
             <li>
                ${requestScope.child.age}                
            </li>
            
        </ul>
    </body>
</html>
