insert ignore into sec_group(groupname, groupdesc) values('DISC_JOCKEYS','This group contains disc jockeys.');
insert ignore into sec_group(groupname, groupdesc) values('LISTENERS','This group contains listeners.');

insert ignore into sec_user(username, password, enabled) values('dj1', SHA2('dj1', 256), true); 
insert ignore into sec_user(username, password, enabled) values('dj2', SHA2('dj2', 256), true); 
insert ignore into sec_user(username, password, enabled) values('listener1', SHA2('listener1', 256), true); 
insert ignore into sec_user(username, password, enabled) values('listener2', SHA2('listener2', 256), true); 

insert ignore into sec_user_groups(username, groupname) values('dj1','DISC_JOCKEYS');
insert ignore into sec_user_groups(username, groupname) values('dj2','DISC_JOCKEYS');
insert ignore into sec_user_groups(username, groupname) values('listener1','LISTENERS');
insert ignore into sec_user_groups(username, groupname) values('listener2','LISTENERS');

insert into listener(id, name, username) values(98, 'Listener One', 'listener1');
insert into listener(id, name, username) values(99, 'Listener Two', 'listener2');

insert into radiostation(id, date_founded, name) values (98,'1973-02-01','WIIT');
insert into radiostation(id, date_founded, name) values (99,'1969-07-01','WNUR');

insert into discjockey(id, datehired, email, name, rs_id, username) values (98, '1980-08-01', 'spyrison@iit.edu', 'DJ Instructorman', 98, 'dj1');
insert into discjockey(id, datehired, email, name, rs_id, username) values (99, '1975-03-01', 'spyrison@iit.edu', 'DJ Musicstudent', 99, 'dj2');

insert into radio_show(id, name) values (98,'Expand Your Mind with John Coltrane');
insert into radio_show(id, name) values (99,'Expand Your Mind with Charles Mingus');

insert into dj_show(dj_id, show_id) values (98,99);
insert into dj_show(dj_id, show_id) values (99,98);