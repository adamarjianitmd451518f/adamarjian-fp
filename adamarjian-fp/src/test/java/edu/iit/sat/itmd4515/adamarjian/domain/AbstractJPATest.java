/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.adamarjian.domain;


import javax.imageio.spi.ServiceRegistry;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.eclipse.persistence.sessions.factories.SessionFactory;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 *
 * @author adama
 */
public class AbstractJPATest {
    protected static EntityManagerFactory emf;
    protected static Validator validator;
    protected EntityManager em;
    protected EntityTransaction tx;
    
 

    //this we want to do once at the class level
    @BeforeClass
    public static void beforeClassTestFixture() {
        emf = Persistence.createEntityManagerFactory("itmd4515PU_TEST");
        ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
        validator = vf.getValidator();
        
 
    }

    @AfterClass
    public static void afterClassTestFixture() {
        emf.close();
    }
    
    @Before 
    public void beforeEachTest(){
        em = emf.createEntityManager();
        tx = em.getTransaction();         
    } 
    
    @After
    public void afterEachTest(){
        if(em != null){
            em.close();
        }
    }
}
