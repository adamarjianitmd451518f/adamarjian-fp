/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.adamarjian.domain;

import java.util.Set;
import javax.validation.ConstraintViolation;
import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;

/**
 *
 * @author adama
 */
public class ChildTest extends AbstractJPATest {

    //in this function we are going to test out Many to one relationship
//by finding our child and setting our guardian name
    @Test
    public void persitPlayer() {
        Game game = new Game("Galaxy Wars");
        Parent parent = new Parent("Elias Damarjian");
        Child child = new Child("Waad Nader", 27, "fuss@email.com");
        Child child2 = new Child("Tom Bomb", 33, "fuss@email.com");
//        we are adding our game to the childs_ID slot in the game table database
//      also refered to as populating the collection
        //       game.setChild(child);
        //     child.getGame().add(game);
        //we are going to set the child to Waad to the game
        game.setChild(child);

        tx.begin();
        em.persist(child);
        em.persist(game);
        em.persist(child2);
        em.persist(parent);
        tx.commit();

        //we are grabing the first element in our table
        //get the name of our players
        //this is from the owning side
        Child findChild = em.find(Child.class, 1l);
        System.out.println("Players name is: " + findChild.getChildName());
        System.out.println("Another way of getting the childs name: "
                + findChild.getGame().get(0).getGameName());
        System.out.println("Another way of getting the childs name: "
                + findChild.getChildName());

        //this is from the inverse side
        Game findMe = em.find(Game.class, 1l);
//        System.out.println("Game name is: " + findMe.getChild().getChildName());
//        System.out.println("Childs name is: " + findMe.getGameName());
//      we are checking to see if the size of the child collection located
//      in parents is at least equal to one
        assertTrue(parent.getChildren().size() <= 1);

    }

//    @Test
    public void shouldFindWaadNader() throws Exception {
        Child child = em.find(Child.class, 1l);
        assertEquals("Waad Nader", child.getChildName());
    }

 //   @Test
    public void shouldCreateAlexAccount() throws Exception {
        //create an instance of Alex Account
        Child child = new Child("Alex Damarjian", 39, "fuss@email.com");

        //persist the child to the database
        tx.begin();
        em.persist(child);

        tx.commit();
        assertNotNull("ID Should not be Null", child.getId());

        //retrieve all of the children from the database
        child = em.createNamedQuery("findNameAlex", Child.class)
                .getSingleResult();
        assertEquals("Alex Damarjian", child.getChildName());
    }

    //  @Test
    public void SetChildToParentName() {
//        Guardian gd = new Guardian("phillip", "Alice");
        //we get the child from our database at 1
        Child child = em.find(Child.class, 1l);
        //find the parent at the database slot at 1
//        Guardian guardian = em.find(Guardian.class, 1l);

        //set the guardian at slot 1 to the child
//        child.setGuardian(guardian);
        tx.begin();
        em.persist(child);
//        em.persist(gd);
        tx.commit();

    }

}
